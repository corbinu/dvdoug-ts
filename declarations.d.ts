declare module "*.json" {
    const value: any;
    export default value;
}

declare module "lab" {
    export type LabTaskDone = (err?: any) => void;

    export interface ILabTestDone {
        (err?: any): void;
        note(note: string): void;
    }

    export interface ILabOptions {
        schedule?: boolean;
        cli?: any;
    }

    export interface ILabOperationOptions {
        timeout?: number;
    }

    export interface ILabExperimentOptions extends ILabOperationOptions {
        parallel?: boolean;
        skip?: boolean;
        only?: boolean;
    }

    export interface ILabTestOptions extends ILabExperimentOptions {
        plan?: number;
    }

    interface ILabExperimentFunction {
        (name: string, experiment: () => void): void;
        (name: string, options: ILabExperimentOptions, experiment: () => void): void;
    }

    export interface ILabExperiment extends ILabExperimentFunction {
        only: ILabExperimentFunction;
        skip: ILabExperimentFunction;
    }

    export interface ILabOperation {
        <T>(name: string, operation: () => Promise<T>): void;
        <T>(name: string, options: ILabOperationOptions, operation: () => Promise<T>): void;

        (name: string, operation: (done: LabTaskDone) => void): void;
        (name: string, options: ILabOperationOptions, operation: (done: LabTaskDone) => void): void;
    }

    export type LabOnCleanup = (next: () => void) => void;

    interface ILabTestFunction {
        <T>(name: string, test: (done?: ILabTestDone, onCleanup?: LabOnCleanup) => Promise<T>): void;
        <T>(name: string, options: ILabTestOptions, test: (done?: ILabTestDone, onCleanup?: LabOnCleanup) => Promise<T>): void;

        (name: string, test: (done: ILabTestDone, onCleanup?: LabOnCleanup) => void): void;
        (name: string, options: ILabTestOptions, test: (done: ILabTestDone, onCleanup?: LabOnCleanup) => void): void;
    }

    export interface ILabTest extends ILabTestFunction {
        only: ILabTestFunction;
        skip: ILabTestFunction;
    }

    export interface ILab {
        experiment: ILabExperiment;
        describe: ILabExperiment;
        suite: ILabExperiment;

        before: ILabOperation;
        beforeEach: ILabOperation;

        test: ILabTest
        it: ILabTest

        afterEach: ILabOperation;
        after: ILabOperation;
    }

    export const assertions: any;

    export function script(options?: ILabOptions): ILab;

    export interface ICodeExpect {
        a: ICodeExpect;
        an: ICodeExpect;
        and: ICodeExpect;
        at: ICodeExpect;
        be: ICodeExpect;
        have: ICodeExpect;
        in: ICodeExpect;
        to: ICodeExpect;

        not: ICodeExpect;
        once: ICodeExpect;
        only: ICodeExpect;
        part: ICodeExpect;
        shallow: ICodeExpect;


        arguments(): ICodeExpect;
        array(): ICodeExpect;
        boolean(): ICodeExpect;
        buffer(): ICodeExpect;
        date(): ICodeExpect;
        error(message?: string | RegExp): ICodeExpect;
        error(type: ErrorConstructor, message?: string | RegExp): ICodeExpect;
        function(): ICodeExpect;
        number(): ICodeExpect;
        regexp(): ICodeExpect;
        string(): ICodeExpect;
        object(): ICodeExpect;

        true(): ICodeExpect;
        false(): ICodeExpect;
        null(): ICodeExpect;
        undefined(): ICodeExpect;

        include(values: any): ICodeExpect;
        includes(values: any): ICodeExpect;
        contain(values: any): ICodeExpect;
        contains(values: any): ICodeExpect;

        startWith(value: string): ICodeExpect;
        startsWith(value: string): ICodeExpect;
        endWith(value: string): ICodeExpect;
        endsWith(value: string): ICodeExpect;

        exist(): ICodeExpect;
        empty(): ICodeExpect;

        length(size: number): ICodeExpect;

        equal(value: any, options?: { prototype?: boolean }): ICodeExpect;

        above(value: number): ICodeExpect;
        greaterThan(value: any): ICodeExpect;

        least(value: any): ICodeExpect;
        min(value: any): ICodeExpect;

        below(value: any): ICodeExpect;
        lessThan(value: any): ICodeExpect;

        most(value: any): ICodeExpect;
        max(value: any): ICodeExpect;

        within(from: any, to: any): ICodeExpect;
        range(from: any, to: any): ICodeExpect;
        between(from: any, to: any): ICodeExpect;

        about(value: any, delta: any): ICodeExpect;

        instanceof(type: ObjectConstructor): ICodeExpect;

        match(regex: RegExp): ICodeExpect;
        matches(regex: RegExp): ICodeExpect;

        satisfy(validator: (value: any) => boolean): ICodeExpect;
        satisfies(validator: (value: any) => boolean): ICodeExpect;

        throw(message?: string | RegExp): ICodeExpect;
        throw(type: ObjectConstructor, message?: string | RegExp): ICodeExpect;
        throws(message?: string | RegExp): ICodeExpect;
        throws(type: ObjectConstructor, message?: string | RegExp): ICodeExpect;
    }

    export function expect(value: any, prefix?: string): ICodeExpect;

    export function fail(message: string): void;

    export function count(): number;

    export function incomplete(): null | string[];

    export function thrownAt(error?: Error): {
        filename: string,
        line: string,
        column: string
    };

    export const settings: {
        truncateMessages: boolean;
        omparePrototypes: boolean;
    }
}