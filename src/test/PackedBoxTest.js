import { script, expect } from "lab";

import ItemList from "../lib/ItemList";
import PackedBox from "../lib/PackedBox";

import TestBox from "./helpers/TestBox";
import TestItem from "./helpers/TestItem";

const lab = script();

//
//Box packing (3D bin packing, knapsack problem)
//@package BoxPacker
//@author Doug Wright
//
lab.experiment("PackedBoxTest", () => {
    lab.test("testGetters", (done) => {
        const box = new TestBox("Box", 370, 375, 60, 140, 364, 374, 40, 3000);
        const item = new TestItem("Item", 230, 330, 6, 320, true);
        const boxItems = new ItemList();

        boxItems.insert(item);
        const packedBox = new PackedBox(box, boxItems, 1, 2, 3, 4, 0, 0, 0);

        expect(1).to.equal(packedBox.getRemainingWidth());
        expect(2).to.equal(packedBox.getRemainingLength());
        expect(3).to.equal(packedBox.getRemainingDepth());
        expect(4).to.equal(packedBox.getRemainingWeight());

        done();
    });

    lab.test("testVolumeUtilisation", (done) => {
        const box = new TestBox("Box", 10, 10, 10, 10, 10, 10, 10, 10);
        const item = new TestItem("Item", 5, 10, 10, 10, true);
        const boxItems = new ItemList();

        boxItems.insert(item);
        const packedBox = new PackedBox(box, boxItems, 1, 2, 3, 4, 0, 0, 0);

        expect(50).to.equal(packedBox.getVolumeUtilisation());

        done();
    });
});

export {
    lab
};
