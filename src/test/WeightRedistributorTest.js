import { script, expect } from "lab";
import * as _ from "lodash";

import BoxList from "../lib/BoxList";
import ItemList from "../lib/ItemList";
import PackedBox from "../lib/PackedBox";
import PackedBoxList from "../lib/PackedBoxList";
import WeightRedistributor from "../lib/WeightRedistributor";

import TestBox from "./helpers/TestBox";
import TestItem from "./helpers/TestItem";

const lab = script();

//
//Box packing (3D bin packing, knapsack problem)
//@package BoxPacker
//@author Doug Wright
//
lab.experiment("WeightRedistributorTest", {"skip": true}, () => {
    lab.test("testWeightRedistribution", (done) => {
        const box = new TestBox("Box", 370, 375, 60, 140, 364, 374, 40, 3000);
        const boxList = new BoxList();

        boxList.insert(box);

        const item1 = new TestItem("Item #1", 230, 330, 6, 320, true);
        const item2 = new TestItem("Item #2", 210, 297, 5, 187, true);
        const item3 = new TestItem("Item #3", 210, 297, 11, 674, true);
        const item4 = new TestItem("Item #4", 210, 297, 3, 82, true);
        const item5 = new TestItem("Item #5", 206, 295, 4, 217, true);

        const box1Items = new ItemList();

        box1Items.insert(_.clone(item1));
        box1Items.insert(_.clone(item1));
        box1Items.insert(_.clone(item1));
        box1Items.insert(_.clone(item1));
        box1Items.insert(_.clone(item1));
        box1Items.insert(_.clone(item1));
        box1Items.insert(_.clone(item5));

        const box2Items = new ItemList();

        box2Items.insert(_.clone(item3));
        box2Items.insert(_.clone(item1));
        box2Items.insert(_.clone(item1));
        box2Items.insert(_.clone(item1));
        box2Items.insert(_.clone(item1));
        box2Items.insert(_.clone(item2));

        const box3Items = new ItemList();

        box3Items.insert(_.clone(item5));
        box3Items.insert(_.clone(item4));

        const packedBox1 = new PackedBox(box, box1Items, 0, 0, 0, 0, 0, 0, 0);
        const packedBox2 = new PackedBox(box, box2Items, 0, 0, 0, 0, 0, 0, 0);
        const packedBox3 = new PackedBox(box, box3Items, 0, 0, 0, 0, 0, 0, 0);
        const packedBoxList = new PackedBoxList();

        packedBoxList.insert(packedBox1);
        packedBoxList.insert(packedBox2);
        packedBoxList.insert(packedBox3);
        const redistributor = new WeightRedistributor(boxList);
        const packedBoxes = redistributor.redistributeWeight(packedBoxList);

        expect(packedBoxes.getWeightVariance()).to.equal(3070);

        done();
    });
});

export {
    lab
};
