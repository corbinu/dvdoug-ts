import { script, expect } from "lab";

import ItemList from "../lib/ItemList";
import PackedBox from "../lib/PackedBox";
import PackedBoxList from "../lib/PackedBoxList";

import TestBox from "./helpers/TestBox";
import TestItem from "./helpers/TestItem";

const lab = script();

//
//Box packing (3D bin packing, knapsack problem)
//@package BoxPacker
//@author Doug Wright
//
lab.experiment("PackedBoxListTest", () => {
    lab.test("testVolumeUtilisation", (done) => {
        const box = new TestBox("Box", 10, 10, 10, 10, 10, 10, 10, 10);
        const item = new TestItem("Item", 5, 10, 10, 10, true);
        const boxItems = new ItemList();

        boxItems.insert(item);

        const packedBox = new PackedBox(box, boxItems, 1, 2, 3, 4, 0, 0, 0);
        const packedBoxList = new PackedBoxList();

        packedBoxList.insert(packedBox);

        expect(packedBoxList.getVolumeUtilisation()).to.equal(50);

        done();
    });

    lab.test("testWeightVariance", (done) => {
        const box = new TestBox("Box", 10, 10, 10, 10, 10, 10, 10, 10);
        const item = new TestItem("Item", 5, 10, 10, 10, true);
        const boxItems = new ItemList();

        boxItems.insert(item);

        const packedBox = new PackedBox(box, boxItems, 1, 2, 3, 4, 0, 0, 0);
        const packedBoxList = new PackedBoxList();

        packedBoxList.insert(packedBox);

        expect(packedBoxList.getWeightVariance()).to.equal(0);

        done();
    });
});

export {
    lab
};
