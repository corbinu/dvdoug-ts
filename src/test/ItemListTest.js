import { script, expect } from "lab";

import ItemList from "../lib/ItemList";

import TestItem from "./helpers/TestItem";

const lab = script();

//
//Box packing (3D bin packing, knapsack problem)
//@package BoxPacker
//@author Doug Wright
//
lab.experiment("ItemListTest", () => {
    lab.test("testCompare", (done) => {
        const box1 = new TestItem("Small", 20, 20, 2, 100, true);
        const box2 = new TestItem("Large", 200, 200, 20, 1000, true);
        const box3 = new TestItem("Medium", 100, 100, 10, 500, true);
        const list = new ItemList();

        list.insert(box1);
        list.insert(box2);
        list.insert(box3);

        const sorted = Array();

        while (!list.empty()) {
            sorted.push(list.pop());
        }

        expect([box2, box3, box1]).to.equal(sorted);

        done();
    });
});

export {
    lab
};
