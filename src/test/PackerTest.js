import { script, expect } from "lab";
import * as _ from "lodash";

import Packer from "../lib/Packer";
import ItemTooLargeError from "../lib/ItemTooLargeError";
//import * as TestData from "../../data/tests.json";

import TestBox from "./helpers/TestBox";
import TestItem from "./helpers/TestItem";

const lab = script();

const boxes = [
    {
        "reference": "Option 1",
        "outerWidth": 230,
        "outerLength": 300,
        "outerDepth": 240,
        "emptyWeight": 160,
        "innerWidth": 230,
        "innerLength": 300,
        "innerDepth": 240,
        "maxWeight": 15000
    },
    {
        "reference": "Option 2",
        "outerWidth": 370,
        "outerLength": 375,
        "outerDepth": 60,
        "emptyWeight": 140,
        "innerWidth": 364,
        "innerLength": 374,
        "innerDepth": 40,
        "maxWeight": 3000
    },
    {
        "reference": "Option 3",
        "outerWidth": 229,
        "outerLength": 305,
        "outerDepth": 520,
        "emptyWeight": 100,
        "innerWidth": 229,
        "innerLength": 305,
        "innerDepth": 520,
        "maxWeight": 10000
    },
    {
        "reference": "Option 4",
        "outerWidth": 240,
        "outerLength": 270,
        "outerDepth": 15,
        "emptyWeight": 10,
        "innerWidth": 220,
        "innerLength": 260,
        "innerDepth": 15,
        "maxWeight": 500
    }
];

//
//Box packing (3D bin packing, knapsack problem)
//@package BoxPacker
//@author Doug Wright
//
lab.experiment("PackerTest", () => {
    lab.test("testPackThreeItemsFitEasilyInSmallerOfTwoBoxes", (done) => {
        const box1 = new TestBox("Le petite box", 300, 300, 10, 10, 296, 296, 8, 1000);
        const box2 = new TestBox("Le grande box", 3000, 3000, 100, 100, 2960, 2960, 80, 10000);
        const item1 = new TestItem("Item 1", 250, 250, 2, 200, true);
        const item2 = new TestItem("Item 2", 250, 250, 2, 200, true);
        const item3 = new TestItem("Item 3", 250, 250, 2, 200, true);
        const packer = new Packer();

        packer.addBox(box1);
        packer.addBox(box2);
        packer.addItem(item1);
        packer.addItem(item2);
        packer.addItem(item3);
        const packedBoxes = packer.pack();

        expect(1).to.equal(packedBoxes.size());
        expect(3).to.equal(packedBoxes.top().getItems().size());
        expect(box1).to.equal(packedBoxes.top().getBox());
        expect(610).to.equal(packedBoxes.top().getWeight());

        done();
    });

    lab.test("testPackThreeItemsFitEasilyInLargerOfTwoBoxes", (done) => {
        const box1 = new TestBox("Le petite box", 300, 300, 10, 10, 296, 296, 8, 1000);
        const box2 = new TestBox("Le grande box", 3000, 3000, 100, 100, 2960, 2960, 80, 10000);
        const item1 = new TestItem("Item 1", 2500, 2500, 20, 2000, true);
        const item2 = new TestItem("Item 2", 2500, 2500, 20, 2000, true);
        const item3 = new TestItem("Item 3", 2500, 2500, 20, 2000, true);
        const packer = new Packer();

        packer.addBox(box1);
        packer.addBox(box2);
        packer.addItem(item1);
        packer.addItem(item2);
        packer.addItem(item3);
        const packedBoxes = packer.pack();

        expect(1).to.equal(packedBoxes.size());
        expect(3).to.equal(packedBoxes.top().getItems().size());
        expect(box2).to.equal(packedBoxes.top().getBox());
        expect(6100).to.equal(packedBoxes.top().getWeight());

        done();
    });

    lab.test("testPackFiveItemsTwoLargeOneSmallBox", (done) => {
        const box1 = new TestBox("Le petite box", 600, 600, 10, 10, 596, 596, 8, 1000);
        const box2 = new TestBox("Le grande box", 3000, 3000, 50, 100, 2960, 2960, 40, 10000);
        const item1 = new TestItem("Item 1", 2500, 2500, 20, 500, true);
        const item2 = new TestItem("Item 2", 550, 550, 2, 500, true);
        const item3 = new TestItem("Item 3", 2500, 2500, 20, 500, true);
        const item4 = new TestItem("Item 4", 2500, 2500, 20, 500, true);
        const item5 = new TestItem("Item 5", 2500, 2500, 20, 500, true);
        const packer = new Packer();

        packer.addBox(box1);
        packer.addBox(box2);
        packer.addItem(item1);
        packer.addItem(item2);
        packer.addItem(item3);
        packer.addItem(item4);
        packer.addItem(item5);

        const packedBoxes = packer.pack();

        expect(packedBoxes.size()).to.equal(3);
        expect(packedBoxes.top().getItems().size()).to.equal(2);
        expect(packedBoxes.top().getBox()).to.equal(box2);
        expect(packedBoxes.top().getWeight()).to.equal(1100);

        packedBoxes.pop();

        expect(packedBoxes.top().getItems().size()).to.equal(2);
        expect(packedBoxes.top().getBox()).to.equal(box2);
        expect(packedBoxes.top().getWeight()).to.equal(1100);

        packedBoxes.pop();

        expect(packedBoxes.top().getItems().size()).to.equal(1);
        expect(packedBoxes.top().getBox()).to.equal(box1);
        expect(packedBoxes.top().getWeight()).to.equal(510);

        done();
    });

    lab.test("testPackFiveItemsTwoLargeOneSmallBoxButThreeAfterRepack", (done) => {
        const box1 = new TestBox("Le petite box", 600, 600, 10, 10, 596, 596, 8, 1000);
        const box2 = new TestBox("Le grande box", 3000, 3000, 50, 100, 2960, 2960, 40, 10000);
        const item1 = new TestItem("Item 1", 2500, 2500, 20, 2000, true);
        const item2 = new TestItem("Item 2", 550, 550, 2, 200, true);
        const item3 = new TestItem("Item 3", 2500, 2500, 20, 2000, true);
        const item4 = new TestItem("Item 4", 2500, 2500, 20, 2000, true);
        const item5 = new TestItem("Item 5", 2500, 2500, 20, 2000, true);
        const packer = new Packer();

        packer.addBox(box1);
        packer.addBox(box2);
        packer.addItem(item1);
        packer.addItem(item2);
        packer.addItem(item3);
        packer.addItem(item4);
        packer.addItem(item5);

        const packedBoxes = packer.pack();

        expect(3).to.equal(packedBoxes.size());
        expect(2).to.equal(packedBoxes.top().getItems().size());
        expect(box2).to.equal(packedBoxes.top().getBox());
        expect(4100).to.equal(packedBoxes.top().getWeight());

        packedBoxes.pop();

        expect(2).to.equal(packedBoxes.top().getItems().size());
        expect(box2).to.equal(packedBoxes.top().getBox());
        expect(2300).to.equal(packedBoxes.top().getWeight());

        packedBoxes.pop();

        expect(1).to.equal(packedBoxes.top().getItems().size());
        expect(box2).to.equal(packedBoxes.top().getBox());
        expect(2100).to.equal(packedBoxes.top().getWeight());

        done();
    });

    lab.test("testPackThreeItemsOneDoesntFitInAnyBox", (done) => {
        const box1 = new TestBox("Le petite box", 300, 300, 10, 10, 296, 296, 8, 1000);
        const box2 = new TestBox("Le grande box", 3000, 3000, 100, 100, 2960, 2960, 80, 10000);
        const item1 = new TestItem("Item 1", 2500, 2500, 20, 2000, true);
        const item2 = new TestItem("Item 2", 25000, 2500, 20, 2000, true);
        const item3 = new TestItem("Item 3", 2500, 2500, 20, 2000, true);
        const packer = new Packer();

        packer.addBox(box1);
        packer.addBox(box2);
        packer.addItem(item1);
        packer.addItem(item2);
        packer.addItem(item3);

        try {
            packer.pack();
        } catch (err) {
            expect(err).to.be.an.error(ItemTooLargeError, "Item Item 2 is too large to fit into any box");
        }

        done();
    });

    lab.test("testPackWithoutBox", (done) => {
        const item1 = new TestItem("Item 1", 2500, 2500, 20, 2000, true);
        const item2 = new TestItem("Item 2", 25000, 2500, 20, 2000, true);
        const item3 = new TestItem("Item 3", 2500, 2500, 20, 2000, true);
        const packer = new Packer();

        packer.addItem(item1);
        packer.addItem(item2);
        packer.addItem(item3);

        try {
            packer.pack();
        } catch (err) {
            expect(err).to.be.an.error(ItemTooLargeError, "Item Item 2 is too large to fit into any box");
        }

        done();
    });

    lab.test("testIssue1", (done) => {
        const packer = new Packer();

        packer.addBox(new TestBox("Le petite box", 292, 336, 60, 10, 292, 336, 60, 9000));
        packer.addBox(new TestBox("Le grande box", 421, 548, 335, 100, 421, 548, 335, 10000));
        packer.addItem(new TestItem("Item 1", 226, 200, 40, 440, true));
        packer.addItem(new TestItem("Item 2", 200, 200, 155, 1660, true));
        const packedBoxes = packer.pack();

        expect(1).to.equal(packedBoxes.size());

        done();
    });

    lab.test("testIssue3", (done) => {
        const packer = new Packer();

        packer.addBox(new TestBox("OW Box 1", 51, 33, 33, 0.6, 51, 33, 33, 0.6));
        packer.addBox(new TestBox("OW Box 2", 50, 40, 40, 0.95, 50, 40, 40, 0.95));
        packer.addItem(new TestItem("Product", 28, 19, 9, 0, true), 6);
        const packedBoxes = packer.pack();

        expect(1).to.equal(packedBoxes.size());

        done();
    });

    lab.test("testIssue6", (done) => {
        const packer = new Packer();

        packer.addBox(new TestBox("Package 22", 675, 360, 210, 2, 670, 355, 204, 1000));
        packer.addBox(new TestBox("Package 2", 330, 130, 102, 2, 335, 135, 107, 1000));
        packer.addItem(new TestItem("Item 3", 355.6, 335.28, 127, 1.5, true));
        packer.addItem(new TestItem("Item 7", 330.2, 127, 101.6, 1, true));
        packer.addItem(new TestItem("Item 7", 330.2, 127, 101.6, 1, true));
        const packedBoxes = packer.pack();

        expect(1).to.equal(packedBoxes.size());

        done();
    });

    lab.test("testIssue9", (done) => {
        const packer = new Packer();

        packer.addBox(new TestBox("24x24x24Box", 24, 24, 24, 24, 24, 24, 24, 100));
        packer.addItem(new TestItem("6x6x6Item", 6, 6, 6, 1, true), 64);
        const packedBoxes = packer.pack();

        expect(1).to.equal(packedBoxes.size());

        done();
    });

    lab.test("testIssue11", (done) => {
        const packer = new Packer();

        packer.addBox(new TestBox("4x4x4Box", 4, 4, 4, 4, 4, 4, 4, 100));
        packer.addItem(new TestItem("BigItem", 2, 2, 4, 1, true), 2);
        packer.addItem(new TestItem("SmallItem", 1, 1, 1, 1, true), 32);
        const packedBoxes = packer.pack();

        expect(1).to.equal(packedBoxes.size());

        done();
    });

    lab.test("testIssue13", (done) => {
        const packer = new Packer();

        packer.addBox(new TestBox("Le petite box", 12, 12, 12, 10, 10, 10, 10, 1000));
        packer.addItem(new TestItem("Item 1", 5, 3, 2, 2, true));
        packer.addItem(new TestItem("Item 2", 5, 3, 2, 2, true));
        packer.addItem(new TestItem("Item 3", 3, 3, 3, 3, true));
        const packedBoxes = packer.pack();

        expect(1).to.equal(packedBoxes.size());

        done();
    });

    lab.test("testIssue14", (done) => {
        const packer = new Packer();

        packer.addBox(new TestBox("29x1x23Box", 29, 1, 23, 0, 29, 1, 23, 100));
        packer.addItem(new TestItem("13x1x10Item", 13, 1, 10, 1, true));
        packer.addItem(new TestItem("9x1x6Item", 9, 1, 6, 1, true));
        packer.addItem(new TestItem("9x1x6Item", 9, 1, 6, 1, true));
        packer.addItem(new TestItem("9x1x6Item", 9, 1, 6, 1, true));
        const packedBoxes = packer.pack();

        expect(1).to.equal(packedBoxes.size());

        done();
    });

    lab.test("testIssue47A", (done) => {
        const packer = new Packer();

        packer.addBox(new TestBox("165x225x25Box", 165, 225, 25, 0, 165, 225, 25, 100));
        packer.addItem(new TestItem("20x69x20Item", 20, 69, 20, 0, true), 23);
        const packedBoxes = packer.pack();

        expect(1).to.equal(packedBoxes.size());

        done();
    });

    lab.test("testIssue47B", (done) => {
        const packer = new Packer();

        packer.addBox(new TestBox("165x225x25Box", 165, 225, 25, 0, 165, 225, 25, 100));
        packer.addItem(new TestItem("20x69x20Item", 69, 20, 20, 0, true), 23);
        const packedBoxes = packer.pack();

        expect(packedBoxes.size()).to.equal(1);

        done();
    });

    lab.test("testIssue47C", { "skip": true }, (done) => {
        const packer = new Packer();

        packer.addBox(new TestBox("Box", 11.75, 23.6875, 3, 0, 11.75, 23.6875, 3, 70));
        packer.addItem(new TestItem("Item", 3.75, 6.5, 3, 0, true), 9);
        const packedBoxes = packer.pack();

        expect(packedBoxes.size()).to.equal(1);

        done();
    });

    lab.test("testIssue47D", (done) => {
        const packer = new Packer();

        packer.addBox(new TestBox("Box", 11.75, 23.6875, 3, 0, 11.75, 23.6875, 3, 70));
        packer.addItem(new TestItem("Item", 6.5, 3.75, 3, 0, true), 9);
        const packedBoxes = packer.pack();

        expect(packedBoxes.size()).to.equal(1);

        done();
    });

    lab.test("testPackerPacksRotatedBoxesInNewRow", (done) => {
        //Box can hold 7 items in a row and then is completely full, so 9 items won"t fit
        //Box can hold 7 items in a row, plus two more rotated, making 9 items
        //with a 10x10x30 hole in the corner.
        //
        //Overhead view:
        //
        //+--+--++
        //++++++++
        //||||||||
        //++++++++
        //
        //Make sure that it doesn"t try to fit in a 10th item
        let packer = new Packer();

        packer.addItem(new TestItem("30x10x30item", 30, 10, 30, 0, true), 9);
        packer.addBox(new TestBox("30x70x30InternalBox", 30, 70, 30, 0, 30, 70, 30, 0, 1000));
        let packedBoxes = packer.pack();

        expect(packedBoxes.size()).to.equal(2);

        packer = new Packer();
        packer.addItem(new TestItem("30x10x30item", 30, 10, 30, 0, true), 9);
        packer.addBox(new TestBox("40x70x30InternalBox", 40, 70, 30, 0, 40, 70, 30, 0, 1000));
        packedBoxes = packer.pack();

        expect(packedBoxes.size()).to.equal(1);

        packer = new Packer();
        packer.addItem(new TestItem("30x10x30item", 30, 10, 30, 0, true), 10);
        packer.addBox(new TestBox("40x70x30InternalBox", 40, 70, 30, 0, 40, 70, 30, 0, 1000));
        packedBoxes = packer.pack();

        expect(packedBoxes.size()).to.equal(2);

        done();
    });

    lab.test("testIssue52A", (done) => {
        const packer = new Packer();

        packer.addBox(new TestBox("Box", 100, 50, 50, 0, 100, 50, 50, 5000));
        packer.addItem(new TestItem("Item", 15, 13, 8, 407, true), 2);
        const packedBoxes = packer.pack();

        expect(packedBoxes.size()).to.equal(1);
        expect(packedBoxes.top().getUsedWidth()).to.equal(15);
        expect(packedBoxes.top().getUsedLength()).to.equal(26);
        expect(packedBoxes.top().getUsedDepth()).to.equal(8);

        done();
    });

    lab.test("testIssue52B", { "skip": true }, (done) => {
        const packer = new Packer();

        packer.addBox(new TestBox("Box", 370, 375, 60, 140, 364, 374, 40, 3000));
        packer.addItem(new TestItem("Item 1", 220, 310, 12, 679, true));
        packer.addItem(new TestItem("Item 2", 210, 297, 11, 648, true));
        packer.addItem(new TestItem("Item 3", 210, 297, 5, 187, true));
        packer.addItem(new TestItem("Item 4", 148, 210, 32, 880, true));
        const packedBoxes = packer.pack();

        expect(packedBoxes.size()).to.equal(1);
        expect(packedBoxes.top().getUsedWidth()).to.equal(310);
        expect(packedBoxes.top().getUsedLength()).to.equal(368);
        expect(packedBoxes.top().getUsedDepth()).to.equal(32);

        done();
    });

    lab.test("testIssue52C", (done) => {
        const packer = new Packer();

        packer.addBox(new TestBox("Box", 230, 300, 240, 160, 230, 300, 240, 15000));
        packer.addItem(new TestItem("Item 1", 210, 297, 4, 213, true));
        packer.addItem(new TestItem("Item 2", 80, 285, 70, 199, true));
        packer.addItem(new TestItem("Item 3", 80, 285, 70, 199, true));

        const packedBoxes = packer.pack();

        expect(2).to.equal(packedBoxes.size());

        const box1 = packedBoxes.pop();
        const box2 = packedBoxes.pop();

        expect(80).to.equal(box1.getUsedWidth());
        expect(285).to.equal(box1.getUsedLength());
        expect(70).to.equal(box1.getUsedDepth());
        expect(210).to.equal(box2.getUsedWidth());
        expect(297).to.equal(box2.getUsedLength());
        expect(4).to.equal(box2.getUsedDepth());

        done();
    });

    lab.experiment("Large 2D sample", () => {

        _.forEach([
            {
                "items": [
                    {
                        "qty": 30,
                        "name": "089b98371aae0ad0d5c861293cd70c9f",
                        "width": 230,
                        "length": 230,
                        "depth": 6,
                        "weight": 320
                    },
                    {
                        "qty": 30,
                        "name": "4df34c2bb19da52e5d68f142f6cf4667",
                        "width": 210,
                        "length": 210,
                        "depth": 4,
                        "weight": 208
                    },
                    {
                        "qty": 4,
                        "name": "ad99cb07e6551966bf1d0feffad68f1b",
                        "width": 80,
                        "length": 80,
                        "depth": 70,
                        "weight": 199
                    }
                ],
                "expected2D": 9,
                "expected3D": 9,
                "weightVariance2D": 169472,
                "weightVariance3D": 169472,
                "name": "1e3576b097febae61d71b70ae74c1c91"
            }
        ], (data) => {
            const { name, items, expected2D, weightVariance2D } = data;

            lab.test(name, (done) => {
                let expectedItemCount = 0;
                let packedItemCount = 0;
                const packer = new Packer();

                for (const box of boxes) {
                    packer.addBox(new TestBox(box.reference, box.outerWidth, box.outerLength, box.outerDepth, box.emptyWeight, box.innerWidth, box.innerLength, box.innerDepth, box.maxWeight));
                }

                for (const item of items) {
                    packer.addItem(new TestItem(item.name, item.width, item.length, item.depth, item.weight, true), item.qty);
                    expectedItemCount += item.qty;
                }

                const packedBoxes = packer.pack();

                for (const packedBox of packedBoxes.toArray()) {
                    packedItemCount += packedBox.getItems().size();
                }

                console.log(name);

                expect(packedBoxes.size()).to.equal(expected2D);
                expect(packedItemCount).to.equal(expectedItemCount);
                expect(packedBoxes.getWeightVariance()).to.equal(weightVariance2D);

                done();
            });

        });

    });

    /*

    lab.experiment("Large 3D sample", () => {

        _.forEach(TestData, (data) => {
            const { name, items, expected3D, weightVariance3D } = data;

            lab.test(name, (done) => {
                let expectedItemCount = 0;
                let packedItemCount = 0;
                const packer = new Packer();

                for (const box of boxes) {
                    packer.addBox(new TestBox(box.reference, box.outerWidth, box.outerLength, box.outerDepth, box.emptyWeight, box.innerWidth, box.innerLength, box.innerDepth, box.maxWeight));
                }

                for (const item of items) {
                    packer.addItem(new TestItem(item.name, item.width, item.length, item.depth, item.weight, false), item.qty);
                    expectedItemCount += item.qty;
                }

                const packedBoxes = packer.pack();

                for (const packedBox of packedBoxes.toArray()) {
                    packedItemCount += packedBox.getItems().size();
                }

                console.log(name);

                expect(packedBoxes.size()).to.equal(expected3D);
                expect(packedItemCount).to.equal(expectedItemCount);
                expect(packedBoxes.getWeightVariance()).to.equal(weightVariance3D);

                done();
            });

        });

    });

    */
});

export {
    lab
};
