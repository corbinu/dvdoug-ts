import { script, expect } from "lab";

import Packer from "../lib/Packer";
import ItemList from "../lib/ItemList";
import VolumePacker from "../lib/VolumePacker";

import TestBox from "./helpers/TestBox";
import TestItem from "./helpers/TestItem";
import TestConstrainedTestItem from "./helpers/TestConstrainedTestItem";

const lab = script();

//
//Box packing (3D bin packing, knapsack problem)
//@package BoxPacker
//@author Doug Wright
//
lab.experiment("VolumePackerTest", () => {
    lab.test("testPackBoxThreeItemsFitEasily", (done) => {
        const box = new TestBox("Le box", 300, 300, 10, 10, 296, 296, 8, 1000);
        const items = new ItemList();

        items.insert(new TestItem("Item 1", 250, 250, 2, 200, false));
        items.insert(new TestItem("Item 2", 250, 250, 2, 200, false));
        items.insert(new TestItem("Item 3", 250, 250, 2, 200, false));
        const packer = new VolumePacker(box, items);
        const packedBox = packer.pack();

        expect(packedBox.getItems().size()).to.equal(3);

        done();
    });

    lab.test("testPackBoxThreeItemsFitExactly", (done) => {
        const box = new TestBox("Le box", 300, 300, 10, 10, 296, 296, 8, 1000);
        const items = new ItemList();

        items.insert(new TestItem("Item 1", 296, 296, 2, 200, false));
        items.insert(new TestItem("Item 2", 296, 296, 2, 500, false));
        items.insert(new TestItem("Item 3", 296, 296, 4, 290, false));
        const packer = new VolumePacker(box, items);
        const packedBox = packer.pack();

        expect(packedBox.getItems().size()).to.equal(3);

        done();
    });

    lab.test("testPackBoxThreeItemsFitExactlyNoRotation", (done) => {
        const box = new TestBox("Le box", 300, 300, 10, 10, 296, 296, 8, 1000);
        const items = new ItemList();

        items.insert(new TestItem("Item 1", 296, 148, 2, 200, false));
        items.insert(new TestItem("Item 2", 296, 148, 2, 500, false));
        const packer = new VolumePacker(box, items);
        const packedBox = packer.pack();

        expect(packedBox.getItems().size()).to.equal(2);

        done();
    });

    lab.test("testPackBoxThreeItemsFitSizeButOverweight", (done) => {
        const box = new TestBox("Le box", 300, 300, 10, 10, 296, 296, 8, 1000);
        const items = new ItemList();

        items.insert(new TestItem("Item 1", 250, 250, 2, 400, false));
        items.insert(new TestItem("Item 2", 250, 250, 2, 500, false));
        items.insert(new TestItem("Item 3", 250, 250, 2, 200, false));
        const packer = new VolumePacker(box, items);
        const packedBox = packer.pack();

        expect(packedBox.getItems().size()).to.equal(2);

        done();
    });

    lab.test("testPackBoxThreeItemsFitWeightBut2Oversize", (done) => {
        const box = new TestBox("Le box", 300, 300, 10, 10, 296, 296, 8, 1000);
        const items = new ItemList();

        items.insert(new TestItem("Item 1", 297, 296, 2, 200, false));
        items.insert(new TestItem("Item 2", 297, 296, 2, 500, false));
        items.insert(new TestItem("Item 3", 296, 296, 4, 290, false));
        const packer = new VolumePacker(box, items);
        const packedBox = packer.pack();

        expect(packedBox.getItems().size()).to.equal(1);

        done();
    });

    lab.test("testPackTwoItemsFitExactlySideBySide", (done) => {
        const box = new TestBox("Le box", 300, 400, 10, 10, 296, 496, 8, 1000);
        const items = new ItemList();

        items.insert(new TestItem("Item 1", 296, 248, 8, 200, false));
        items.insert(new TestItem("Item 2", 248, 296, 8, 200, false));
        const packer = new VolumePacker(box, items);
        const packedBox = packer.pack();

        expect(packedBox.getItems().size()).to.equal(2);

        done();
    });

    lab.test("testPackThreeItemsBottom2FitSideBySideOneExactlyOnTop", (done) => {
        const box = new TestBox("Le box", 300, 300, 10, 10, 296, 296, 8, 1000);
        const items = new ItemList();

        items.insert(new TestItem("Item 1", 248, 148, 4, 200, false));
        items.insert(new TestItem("Item 2", 148, 248, 4, 200, false));
        items.insert(new TestItem("Item 3", 296, 296, 4, 200, false));
        const packer = new VolumePacker(box, items);
        const packedBox = packer.pack();

        expect(packedBox.getItems().size()).to.equal(3);

        done();
    });

    lab.test("testPackThreeItemsBottom2FitSideBySideWithSpareSpaceOneOverhangSlightlyOnTop", (done) => {
        const box = new TestBox("Le box", 250, 250, 10, 10, 248, 248, 8, 1000);
        const items = new ItemList();

        items.insert(new TestItem("Item 1", 200, 200, 4, 200, false));
        items.insert(new TestItem("Item 2", 110, 110, 4, 200, false));
        items.insert(new TestItem("Item 3", 110, 110, 4, 200, false));
        const packer = new VolumePacker(box, items);
        const packedBox = packer.pack();

        expect(packedBox.getItems().size()).to.equal(3);

        done();
    });

    lab.test("testPackSingleItemFitsBetterRotated", (done) => {
        const box = new TestBox("Le box", 400, 300, 10, 10, 396, 296, 8, 1000);
        const items = new ItemList();

        items.insert(new TestItem("Item 1", 250, 290, 2, 200, false));
        const packer = new VolumePacker(box, items);
        const packedBox = packer.pack();

        expect(packedBox.getItems().size()).to.equal(1);

        done();
    });

    lab.test("testIssue20", (done) => {
        const packer = new Packer();

        packer.addBox(new TestBox("Le grande box", 100, 100, 300, 1, 100, 100, 300, 1500));
        packer.addItem(new TestItem("Item 1", 150, 50, 50, 20, false));
        const packedBoxes = packer.pack();

        expect(packedBoxes.size()).to.equal(1);

        done();
    });

    lab.test("testIssue53", (done) => {
        const packer = new Packer();

        packer.addBox(new TestBox("Box", 500, 1000, 500, 0, 500, 1000, 500, 0));
        packer.addItem(new TestItem("Item 1", 500, 500, 500, 0, false));
        packer.addItem(new TestItem("Item 2", 500, 500, 250, 0, false), 2);
        const packedBoxes = packer.pack();

        expect(packedBoxes.size()).to.equal(1);

        done();
    });

    lab.test("testConstraints", { "skip": true }, (done) => {
        TestConstrainedTestItem.limit = 2;
        const packer = new Packer();

        packer.addBox(new TestBox("Box", 10, 10, 10, 0, 10, 10, 10, 0));
        packer.addItem(new TestConstrainedTestItem("Item", 1, 1, 1, 0, false), 8);
        const packedBoxes = packer.pack();

        expect(packedBoxes.size()).to.equal(4);

        done();
    });
});

export {
    lab
};
