import TestItem from "./TestItem";
//
//Box packing (3D bin packing, knapsack problem)
//@package BoxPacker
//@author Doug Wright
//

//
//@var int
//
//
//@param ItemList $alreadyPackedItems
//@param TestBox  $box
//
//@return bool
//
export default class TestConstrainedTestItem extends TestItem {
    canBePackedInBox(alreadyPackedItems) {
        const alreadyPackedType = alreadyPackedItems.toArray().filter((item) => {
            return item.getDescription() === this.getDescription();
        });

        return alreadyPackedType.length + 1 <= TestConstrainedTestItem.limit;
    }

}

TestConstrainedTestItem.limit = 3;
