//
//Box packing (3D bin packing, knapsack problem)
//@package BoxPacker
//@author Doug Wright
//

//
//@var string
//
//
//@var int
//
//
//@var int
//
//
//@var int
//
//
//@var int
//
//
//@var int
//
//
//@var int
//
//
//TestItem constructor.
//
//@param string $description
//@param int $width
//@param int $length
//@param int $depth
//@param int $weight
//@param int $keepFlat
//
//
//@return string
//
//
//@return int
//
//
//@return int
//
//
//@return int
//
//
//@return int
//
//
//@return int
//
//
//@return int
//
export default class TestItem {
    constructor(description, width, length, depth, weight, keepFlat) {
        this.description = description;
        this.width = width;
        this.length = length;
        this.depth = depth;
        this.weight = weight;
        this.keepFlat = keepFlat;
        this.volume = this.width * (this.length * this.depth);
    }

    getDescription() {
        return this.description;
    }

    getWidth() {
        return this.width;
    }

    getLength() {
        return this.length;
    }

    getDepth() {
        return this.depth;
    }

    getWeight() {
        return this.weight;
    }

    getVolume() {
        return this.volume;
    }

    getKeepFlat() {
        return this.keepFlat;
    }
}
