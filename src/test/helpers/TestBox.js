//
//Box packing (3D bin packing, knapsack problem)
//@package BoxPacker
//@author Doug Wright
//

//
//@var string
//
//
//@var int
//
//
//@var int
//
//
//@var int
//
//
//@var int
//
//
//@var int
//
//
//@var int
//
//
//@var int
//
//
//@var int
//
//
//@var int
//
//
//TestBox constructor.
//
//@param string $reference
//@param int $outerWidth
//@param int $outerLength
//@param int $outerDepth
//@param int $emptyWeight
//@param int $innerWidth
//@param int $innerLength
//@param int $innerDepth
//@param int $maxWeight
//
//
//@return string
//
//
//@return int
//
//
//@return int
//
//
//@return int
//
//
//@return int
//
//
//@return int
//
//
//@return int
//
//
//@return int
//
//
//@return int
//
//
//@return int
//
export default class TestBox {
    constructor(reference, outerWidth, outerLength, outerDepth, emptyWeight, innerWidth, innerLength, innerDepth, maxWeight) {
        this.reference = reference;
        this.outerWidth = outerWidth;
        this.outerLength = outerLength;
        this.outerDepth = outerDepth;
        this.emptyWeight = emptyWeight;
        this.innerWidth = innerWidth;
        this.innerLength = innerLength;
        this.innerDepth = innerDepth;
        this.maxWeight = maxWeight;
        this.innerVolume = this.innerWidth * (this.innerLength * this.innerDepth);
    }

    getReference() {
        return this.reference;
    }

    getOuterWidth() {
        return this.outerWidth;
    }

    getOuterLength() {
        return this.outerLength;
    }

    getOuterDepth() {
        return this.outerDepth;
    }

    getEmptyWeight() {
        return this.emptyWeight;
    }

    getInnerWidth() {
        return this.innerWidth;
    }

    getInnerLength() {
        return this.innerLength;
    }

    getInnerDepth() {
        return this.innerDepth;
    }

    getInnerVolume() {
        return this.innerVolume;
    }

    getMaxWeight() {
        return this.maxWeight;
    }

}
