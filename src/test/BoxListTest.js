import { script, expect } from "lab";

import BoxList from "../lib/BoxList";

import TestBox from "./helpers/TestBox";

const lab = script();

//
//Box packing (3D bin packing, knapsack problem)
//@package BoxPacker
//@author Doug Wright
//
lab.experiment("BoxListTest", () => {
    lab.test("testCompare", (done) => {

        const box1 = new TestBox("Small", 21, 21, 3, 1, 20, 20, 2, 100);
        const box2 = new TestBox("Large", 201, 201, 21, 1, 200, 200, 20, 1000);
        const box3 = new TestBox("Medium", 101, 101, 11, 5, 100, 100, 10, 500);
        const list = new BoxList();

        list.insert(box1);
        list.insert(box2);
        list.insert(box3);
        const sorted = Array();

        while (!list.empty()) {
            sorted.push(list.pop());
        }

        expect([box1, box3, box2]).to.equal(sorted);

        done();
    });

    lab.test("testIssue14A", (done) => {
        const box1 = new TestBox("Small", 21, 21, 3, 1, 20, 20, 2, 100);
        const box2 = new TestBox("Large", 1301, 1301, 1301, 1, 1300, 1300, 1300, 1000);
        const box3 = new TestBox("Medium", 101, 101, 11, 5, 100, 100, 10, 500);
        const list = new BoxList();

        list.insert(box1);
        list.insert(box2);
        list.insert(box3);
        const sorted = Array();

        while (!list.empty()) {
            sorted.push(list.pop());
        }

        expect([box1, box3, box2]).to.equal(sorted);

        done();
    });

    lab.test("testIssue14B", (done) => {
        const box1 = new TestBox("Small", 21, 21, 3, 1, 20, 20, 2, 100);
        const box2 = new TestBox("Large", 1301, 1301, 1301, 1, 1300, 1300, 1300, 1000);
        const box3 = new TestBox("Medium", 101, 101, 11, 5, 100, 100, 10, 500);
        let list = new BoxList();

        list.insert(box3);
        list.insert(box2);
        list.insert(box1);

        let sorted = Array();

        while (!list.empty()) {
            sorted.push(list.pop());
        }

        expect([box1, box3, box2]).to.equal(sorted);

        list = new BoxList();
        list.insert(box2);
        list.insert(box1);
        list.insert(box3);
        sorted = Array();

        while (!list.empty()) {
            sorted.push(list.pop());
        }

        expect([box1, box3, box2]).to.equal(sorted);

        done();
    });
});

export {
    lab
};
