import * as _ from "lodash";
import * as asort from "locutus/php/array/asort";
import * as reset from "locutus/php/array/reset";
import * as key from "locutus/php/array/key";

import OrientatedItem from "./OrientatedItem";
//
//Box packing (3D bin packing, knapsack problem)
//@package BoxPacker
//@author Doug Wright
//

//
//Get the best orientation for an item
//@param Box $box
//@param Item $item
//@param OrientatedItem|null $prevItem
//@param Item|null $nextItem
//@param int $widthLeft
//@param int $lengthLeft
//@param int $depthLeft
//@return OrientatedItem|false
//
//
//Find all possible orientations for an item
//@param Item $item
//@param OrientatedItem|null $prevItem
//@param int $widthLeft
//@param int $lengthLeft
//@param int $depthLeft
//@return OrientatedItem[]
//
//
//@param OrientatedItem[] $possibleOrientations
//@param Box              $box
//@param Item             $item
//@param OrientatedItem   $prevItem
//@param Item             $nextItem
//
//@return array
//
export default class OrientatedItemFactory {
    getBestOrientation(box, item, prevItem, nextItem, widthLeft, lengthLeft, depthLeft) {
        const possibleOrientations = this.getPossibleOrientations(item, prevItem, widthLeft, lengthLeft, depthLeft);
        const usableOrientations = this.getUsableOrientations(possibleOrientations, box, item, prevItem, nextItem);
        const orientationFits = Array();

        for (const o in usableOrientations) {
            if ({}.hasOwnProperty.call(usableOrientations, o)) {
                const orientation = usableOrientations[o];
                const orientationFit = Math.min(widthLeft - orientation.getWidth(), lengthLeft - orientation.getLength());

                orientationFits[o] = orientationFit;
            }
        }

        if (orientationFits) {
            asort(orientationFits);
            reset(orientationFits);
            const bestFit = usableOrientations[key(orientationFits)];

            /*
            this.logger.debug("Selected best fit orientation", {
                "orientation": bestFit
            });
            */

            return bestFit;
        }

        return false;

    }

    getPossibleOrientations(item, prevItem, widthLeft, lengthLeft, depthLeft) {
        //Special case items that are the same as what we just packed - keep orientation
        //remove any that simply don't fit
        const orientations = Array();

        if (prevItem && prevItem.getItem() === item) {
            orientations.push(new OrientatedItem(item, prevItem.getWidth(), prevItem.getLength(), prevItem.getDepth()));
        } else {
            //simple 2D rotation
            //add 3D rotation if we're allowed
            orientations.push(new OrientatedItem(item, item.getWidth(), item.getLength(), item.getDepth()));
            orientations.push(new OrientatedItem(item, item.getLength(), item.getWidth(), item.getDepth()));

            if (!item.getKeepFlat()) {
                orientations.push(new OrientatedItem(item, item.getWidth(), item.getDepth(), item.getLength()));
                orientations.push(new OrientatedItem(item, item.getLength(), item.getDepth(), item.getWidth()));
                orientations.push(new OrientatedItem(item, item.getDepth(), item.getWidth(), item.getLength()));
                orientations.push(new OrientatedItem(item, item.getDepth(), item.getLength(), item.getWidth()));
            }
        }

        return orientations.filter((i) => {
            return i.getWidth() <= widthLeft && (i.getLength() <= lengthLeft && i.getDepth() <= depthLeft);
        });
    }

    getUsableOrientations(possibleOrientations, box, item, prevItem, nextItem) {
        /* Divide possible orientations into stable (low centre of gravity) and unstable (high centre of gravity)
        * We prefer to use stable orientations only, but allow unstable ones if either
        * the item is the last one left to pack OR
        * the item doesn't fit in the box any other way
        */
        const stableOrientations = Array();
        const unstableOrientations = Array();

        for (const o in possibleOrientations) {
            if ({}.hasOwnProperty.call(possibleOrientations, o)) {
                const orientation = possibleOrientations[o];

                if (orientation.isStable()) {
                    stableOrientations.push(orientation);
                } else {
                    unstableOrientations.push(orientation);
                }
            }
        }

        let orientationsToUse = Array();

        if (stableOrientations.length > 0) {
            orientationsToUse = stableOrientations;
        } else if (unstableOrientations.length > 0) {
            const orientationsInEmptyBox = this.getPossibleOrientations(item, prevItem, box.getInnerWidth(), box.getInnerLength(), box.getInnerDepth());
            const stableOrientationsInEmptyBox = orientationsInEmptyBox.filter((orientation) => {
                return orientation.isStable();
            });

            if (_.isNull(nextItem) || stableOrientationsInEmptyBox.length === 0) {
                orientationsToUse = unstableOrientations;
            }
        }

        return orientationsToUse;
    }

}
