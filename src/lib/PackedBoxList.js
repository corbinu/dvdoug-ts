import * as _ from "lodash";

import Heap from "./heap";

function compare(boxA, boxB) {
    let choice = boxB.getItems().size() - boxA.getItems().size();

    if (choice === 0) {
        choice = boxA.getBox().getInnerVolume() - boxB.getBox().getInnerVolume();
    }

    if (choice === 0) {
        choice = boxB.getWeight() - boxA.getWeight();
    }

    return choice;
}

//
//Box packing (3D bin packing, knapsack problem)
//@package BoxPacker
//@author Doug Wright
//

//
//Average (mean) weight of boxes
//@var float
//
//
//Compare elements in order to place them correctly in the heap while sifting up.
//@see \SplMinHeap::compare()
//
//
//Reversed version of compare
//@return int
//
//
//Calculate the average (mean) weight of the boxes
//@return float
//
//
//Calculate the variance in weight between these boxes
//@return float
//
//
//Get volume utilisation of the set of packed boxes
//@return float
//
//
//Do a bulk insert
//@param array $boxes
//
export default class PackedBoxList extends Heap {
    constructor() {
        super(compare);
    }

    reverseCompare(boxA, boxB) {
        let choice = boxA.getItems().size() - boxB.getItems().size();

        if (choice === 0) {
            choice = boxB.getBox().getInnerVolume() - boxA.getBox().getInnerVolume();
        }

        if (choice === 0) {
            choice = boxA.getWeight() - boxB.getWeight();
        }

        return choice;
    }

    getMeanWeight() {
        if (!_.isNull(this.meanWeight) && !_.isUndefined(this.meanWeight)) {
            return this.meanWeight;
        }

        this.meanWeight = 0;

        for (const box of this.toArray()) {
            this.meanWeight += box.getWeight();
        }

        return this.meanWeight /= this.size();
    }

    getWeightVariance() {
        const mean = this.getMeanWeight();
        let weightVariance = 0;

        for (const box of _.clone(this.toArray())) {
            weightVariance += Math.pow(box.getWeight() - mean, 2);
        }

        return Math.round(weightVariance / this.size());
    }

    getVolumeUtilisation() {
        let itemVolume = 0;
        let boxVolume = 0;

        for (const box of this.toArray()) {
            boxVolume += box.getBox().getInnerVolume();

            _.forEach(box.getItems().toArray(), (item) => {
                itemVolume += item.getVolume();
            });
        }

        return Math.round((itemVolume / boxVolume) * 100);
    }

    insertFromArray(boxes) {
        for (const box of boxes) {
            this.insert(box);
        }
    }

}
