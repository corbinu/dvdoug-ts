import * as _ from "lodash";
import * as uuid from "uuid/v4";

export interface IndexSignature {
  [key: string]: any;
}

/*
Min comparison function to be used
*/
function minCmp<T>(x: T, y: T) {
    if (x < y) return -1;
    if (x > y) return 1;

    return 0;
}

/*
Max comparison function to be used
*/
function maxCmp<T>(x: T, y: T) {
    if (x < y) return 1;
    if (x > y) return -1;

    return 0;
}

function siftdown<T>(array: Array<T>, startpos: number, pos: number, cmp: (item1: T, item2: T) => number) {
    const newitem = array[pos];

    while (pos > startpos) {
        const parentpos = (pos - 1) >> 1;
        const parent = array[parentpos];

        if (cmp(newitem, parent) < 0) {
            array[pos] = parent;
            pos = parentpos;
            continue;
        }
        break;
    }

    return array[pos] = newitem;
}

function siftup<T>(array: Array<T>, pos: number, cmp: (item1: T, item2: T) => number) {
    const endpos = array.length;
    const startpos = pos;
    const newitem = array[pos];
    let childpos = (2 * pos) + 1;

    while (childpos < endpos) {
        const rightpos = childpos + 1;

        if ((rightpos < endpos) && !(cmp(array[childpos], array[rightpos]) < 0)) {
            childpos = rightpos;
        }
        array[pos] = array[childpos];
        pos = childpos;
        childpos = (2 * pos) + 1;
    }
    array[pos] = newitem;

    return siftdown(array, startpos, pos, cmp);
}

export default class Heap<T extends IndexSignature> {
    private cmp: (item1: T, item2: T) => number;
    private nodes: Array<T> = [];

    public constructor(cmp: (item1: T, item2: T) => number) {
        this.cmp = cmp;
    }

    public push(x: T): T {
        x[Symbol.for("head-ts-id")] = uuid();

        this.nodes.push(x);

        return siftdown(this.nodes, 0, this.nodes.length - 1, this.cmp);
    }

    public insert(x: T): T {
        return this.push(x);
    }

    public pop(): T | undefined {
        let returnitem;

        const lastelt: T | undefined = this.nodes.pop();

        if (this.nodes.length > 0) {
            [returnitem] = this.nodes;

            if (lastelt !== undefined) this.nodes[0] = lastelt;

            siftup(this.nodes, 0, this.cmp);
        } else {
            returnitem = lastelt;
        }

        return returnitem;
    }

    public top(): T {
        return this.peek();
    }

    public front(): T {
        return this.peek();
    }

    public peek(): T {
        return this.nodes[0];
    }

    public contains(x: T): boolean {
        return _.includes(this.nodes, x);
    }

    public has(x: T): boolean {
        return this.contains(x);
    }

    public replace(x: T): T {
        const [returnitem] = this.nodes;

        if (!x[Symbol.for("head-ts-id")]!) x[Symbol.for("head-ts-id")] = returnitem[Symbol.for("head-ts-id")];

        this.nodes[0] = x;

        siftup(this.nodes, 0, this.cmp);

        return returnitem;
    }

    public pushpop(x: T): T {
        x[Symbol.for("head-ts-id")] = uuid();

        console.log(x[Symbol.for("head-ts-id")]);

        if ((this.nodes.length > 0) && (this.cmp(this.nodes[0], x) < 0)) {
            [x, this.nodes[0]] = [this.nodes[0], x];
            siftup(this.nodes, 0, this.cmp);
        }

        return x;
    }

    public heapify(): any {
        return _.map(_.rangeRight(0, Math.floor(this.nodes.length / 2)), (i: number) => (siftup(this.nodes, i, this.cmp)));
    }

    public updateItem(x: T): T | undefined {
        const pos = this.nodes.indexOf(x);

        if (pos === -1) return;

        x[Symbol.for("head-ts-id")] = this.nodes[pos][Symbol.for("head-ts-id")];

        siftdown(this.nodes, 0, pos, this.cmp);

        return siftup(this.nodes, pos, this.cmp);
    }

    public clear(): void {
        this.nodes = [];
    }

    public empty(): boolean {
        return this.nodes.length === 0;
    }

    public size(): number {
        return this.nodes.length;
    }

    public clone(): Heap<T> {
        const heap = new Heap<T>(this.cmp);

        heap.nodes = _.clone(this.nodes);

        return heap;
    }

    public toArray(): Array<T> {
        return _.clone(this.nodes);
    }
}

export class MinHeap<T> extends Heap<T> {
    public constructor() {
        super(minCmp);
    }
}

export class MaxHeap<T> extends Heap<T> {
    public constructor() {
        super(maxCmp);
    }
}
