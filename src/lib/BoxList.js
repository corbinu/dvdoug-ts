import Heap from "./heap";

function compare(boxA, boxB) {
    if (boxB.getInnerVolume() > boxA.getInnerVolume()) {
        return -1;
    } else if (boxB.getInnerVolume() < boxA.getInnerVolume()) {
        return 1;
    }

    return 0;
}

//
//Box packing (3D bin packing, knapsack problem)
//@package BoxPacker
//@author Doug Wright
//
//
//Compare elements in order to place them correctly in the heap while sifting up.
//@see \SplMinHeap::compare()
//
export default class BoxList extends Heap {
    constructor() {
        super(compare);
    }
}
