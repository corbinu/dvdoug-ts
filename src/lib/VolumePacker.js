import * as _ from "lodash";

import TestConstrainedTestItem from "../test/helpers/TestConstrainedTestItem";

import ItemList from "./ItemList";
import PackedBox from "./PackedBox";
import OrientatedItemFactory from "./OrientatedItemFactory";

//
//Box packing (3D bin packing, knapsack problem)
//@package BoxPacker
//@author Doug Wright
//

//
//Box to pack items into
//@var Box
//
//
//List of items to be packed
//@var ItemList
//
//
//Remaining width of the box to pack items into
//@var int
//
//
//Remaining length of the box to pack items into
//@var int
//
//
//Remaining depth of the box to pack items into
//@var int
//
//
//Remaining weight capacity of the box
//@var int
//
//
//Used width inside box for packing items
//@var int
//
//
//Used length inside box for packing items
//@var int
//
//
//Used depth inside box for packing items
//@var int
//
//
//@var int
//
//
//@var int
//
//
//@var int
//
//
//Constructor
//
//@param Box      $box
//@param ItemList $items
//
//
//Pack as many items as possible into specific given box
//@return PackedBox packed box
//
//
//@param Item $itemToPack
//@param OrientatedItem|null $prevItem
//@param Item|null $nextItem
//@param int $maxWidth
//@param int $maxLength
//@param int $maxDepth
//
//@return OrientatedItem|false
//
//
//Figure out if we can stack the next item vertically on top of this rather than side by side
//Used when we've packed a tall item, and have just put a shorter one next to it
//
//@param ItemList       $packedItems
//@param OrientatedItem $prevItem
//@param Item           $nextItem
//@param int            $maxWidth
//@param int            $maxLength
//@param int            $maxDepth
//
//
//@return bool
//
//
//As well as purely dimensional constraints, there are other constraints that need to be met
//e.g. weight limits or item-specific restrictions (e.g. max <x> batteries per box)
//
//@param Item     $itemToPack
//@param ItemList $packedItems
//
//@return bool
//
export default class VolumePacker {
    constructor(box, items) {
        this.usedWidth = 0;
        this.usedLength = 0;
        this.usedDepth = 0;
        this.layerWidth = 0;
        this.layerLength = 0;
        this.layerDepth = 0;
        //this.logger = new NullLogger();
        this.box = box;
        this.items = items;
        this.depthLeft = this.box.getInnerDepth();
        this.remainingWeight = this.box.getMaxWeight() - this.box.getEmptyWeight();
        this.widthLeft = this.box.getInnerWidth();
        this.lengthLeft = this.box.getInnerLength();
    }

    pack() {
        //this.logger.debug(`[EVALUATING BOX] ${this.box.getReference()}`);
        const packedItems = new ItemList();

        this.layerWidth = 0;
        this.layerLength = 0;
        this.layerDepth = 0;

        let prevItem = undefined;

        while (!this.items.empty()) {
            //skip items that are simply too heavy
            const itemToPack = this.items.pop();

            //console.log("itemToPack");
            //console.log(JSON.stringify(itemToPack));

            if (!this.checkNonDimensionalConstraints(itemToPack, packedItems)) {
                continue;
            }

            let nextItem = undefined;

            if (!this.items.empty()) nextItem = this.items.top();

            //console.log("nextItem");
            //console.log(JSON.stringify(nextItem));

            const orientatedItem = this.getOrientationForItem(itemToPack, prevItem, nextItem, this.widthLeft, this.lengthLeft, this.depthLeft);

            if (orientatedItem) {
                //greater than 0, items will always be less deep
                //allow items to be stacked in place within the same footprint up to current layerdepth
                packedItems.insert(orientatedItem.getItem());
                this.remainingWeight -= orientatedItem.getItem().getWeight();
                this.lengthLeft -= orientatedItem.getLength();
                this.layerLength += orientatedItem.getLength();
                this.layerWidth = Math.max(orientatedItem.getWidth(), this.layerWidth);
                this.layerDepth = Math.max(this.layerDepth, orientatedItem.getDepth());
                this.usedLength = Math.max(this.usedLength, this.layerLength);
                this.usedWidth = Math.max(this.usedWidth, this.layerWidth);
                const stackableDepth = this.layerDepth - orientatedItem.getDepth();

                this.tryAndStackItemsIntoSpace(packedItems, prevItem, nextItem, orientatedItem.getWidth(), orientatedItem.getLength(), stackableDepth);
                prevItem = orientatedItem;

                if (this.items.empty()) {
                    this.usedDepth += this.layerDepth;
                }
            } else {
                prevItem = undefined;

                if (this.widthLeft >= Math.min(itemToPack.getWidth(), itemToPack.getLength()) && this.isLayerStarted()) {
                    //this.logger.debug("No more fit in lengthwise, resetting for new row");
                    this.lengthLeft += this.layerLength;
                    this.widthLeft -= this.layerWidth;
                    this.layerWidth = 0;
                    this.layerLength = 0;
                    this.items.insert(itemToPack);
                    continue;
                } else if (this.lengthLeft < Math.min(itemToPack.getWidth(), itemToPack.getLength()) || this.layerDepth === 0) {
                    //this.logger.debug("doesn't fit on layer even when empty");
                    this.usedDepth += this.layerDepth;
                    continue;
                }

                this.widthLeft = this.box.getInnerWidth();
                if (this.layerWidth) this.widthLeft = Math.min(Math.floor(this.layerWidth * 1.1), this.box.getInnerWidth());

                this.lengthLeft = this.box.getInnerLength();
                if (this.layerLength) this.lengthLeft = Math.min(Math.floor(this.layerLength * 1.1), this.box.getInnerLength());

                this.depthLeft -= this.layerDepth;
                this.usedDepth += this.layerDepth;
                this.layerWidth = 0;
                this.layerLength = 0;
                this.layerDepth = 0;
                //this.logger.debug("doesn't fit, so starting next vertical layer");
                this.items.insert(itemToPack);
            }
        }

        //this.logger.debug("done with this box");

        //console.log("packedItems");
        //console.log(JSON.stringify(packedItems));

        return new PackedBox(this.box, packedItems, this.widthLeft, this.lengthLeft, this.depthLeft, this.remainingWeight, this.usedWidth, this.usedLength, this.usedDepth);
    }

    getOrientationForItem(itemToPack, prevItem, nextItem, maxWidth, maxLength, maxDepth) {
        /*
        this.logger.debug(`evaluating item ${itemToPack.getDescription()} for fit`, {
            "item": itemToPack,
            "space": {
                maxWidth,
                maxLength,
                maxDepth,
                "layerWidth": this.layerWidth,
                "layerLength": this.layerLength,
                "layerDepth": this.layerDepth
            }
        });
        */
        const orientatedItemFactory = new OrientatedItemFactory();

        //orientatedItemFactory.setLogger(this.logger);
        const orientatedItem = orientatedItemFactory.getBestOrientation(this.box, itemToPack, prevItem, nextItem, maxWidth, maxLength, maxDepth);

        return orientatedItem;
    }

    tryAndStackItemsIntoSpace(packedItems, prevItem, nextItem, maxWidth, maxLength, maxDepth) {
        while (!this.items.empty() && this.remainingWeight >= this.items.top().getWeight()) {
            const stackedItem = this.getOrientationForItem(this.items.top(), prevItem, nextItem, maxWidth, maxLength, maxDepth);

            if (stackedItem) {
                this.remainingWeight -= this.items.top().getWeight();
                maxDepth -= stackedItem.getDepth();
                packedItems.insert(this.items.pop());
            } else {
                break;
            }
        }
    }

    isLayerStarted() {
        return this.layerWidth > 0 && this.layerLength > 0 && this.layerDepth > 0;
    }

    checkNonDimensionalConstraints(itemToPack, packedItems) {
        const weightOK = itemToPack.getWeight() <= this.remainingWeight;

        if (itemToPack instanceof TestConstrainedTestItem) {
            return weightOK && itemToPack.canBePackedInBox(_.clone(packedItems), this.box);
        }

        return weightOK;
    }

}
