import * as _ from "lodash";

//
//Box packing (3D bin packing, knapsack problem)
//@package BoxPacker
//@author Doug Wright
//

//
//Box used
//@var Box
//
//
//Items in the box
//@var ItemList
//
//
//Total weight of box
//@var int
//
//
//Remaining width inside box for another item
//@var int
//
//
//Remaining length inside box for another item
//@var int
//
//
//Remaining depth inside box for another item
//@var int
//
//
//Remaining weight inside box for another item
//@var int
//
//
//Used width inside box for packing items
//@var int
//
//
//Used length inside box for packing items
//@var int
//
//
//Used depth inside box for packing items
//@var int
//
//
//Get box used
//@return Box
//
//
//Get items packed
//@return ItemList
//
//
//Get packed weight
//@return int weight in grams
//
//
//Get remaining width inside box for another item
//@return int
//
//
//Get remaining length inside box for another item
//@return int
//
//
//Get remaining depth inside box for another item
//@return int
//
//
//Used width inside box for packing items
//@return int
//
//
//Used length inside box for packing items
//@return int
//
//
//Used depth inside box for packing items
//@return int
//
//
//Get remaining weight inside box for another item
//@return int
//
//
//Get volume utilisation of the packed box
//@return float
//
//
//Constructor
//
//@param Box      $box
//@param ItemList $itemList
//@param int      $remainingWidth
//@param int      $remainingLength
//@param int      $remainingDepth
//@param int      $remainingWeight
//@param int      $usedWidth
//@param int      $usedLength
//@param int      $usedDepth
//
export default class PackedBox {
    constructor(box, itemList, remainingWidth, remainingLength, remainingDepth, remainingWeight, usedWidth, usedLength, usedDepth) {
        this.box = box;
        this.items = itemList;
        this.remainingWidth = remainingWidth;
        this.remainingLength = remainingLength;
        this.remainingDepth = remainingDepth;
        this.remainingWeight = remainingWeight;
        this.usedWidth = usedWidth;
        this.usedLength = usedLength;
        this.usedDepth = usedDepth;
    }

    getBox() {
        return this.box;
    }

    getItems() {
        return this.items;
    }

    getWeight() {
        if (!_.isNull(this.weight) && !_.isUndefined(this.weight)) {
            return this.weight;
        }

        this.weight = this.box.getEmptyWeight();

        for (const item of this.items.toArray()) {
            this.weight += item.getWeight();
        }

        return this.weight;
    }

    getRemainingWidth() {
        return this.remainingWidth;
    }

    getRemainingLength() {
        return this.remainingLength;
    }

    getRemainingDepth() {
        return this.remainingDepth;
    }

    getUsedWidth() {
        return this.usedWidth;
    }

    getUsedLength() {
        return this.usedLength;
    }

    getUsedDepth() {
        return this.usedDepth;
    }

    getRemainingWeight() {
        return this.remainingWeight;
    }

    getVolumeUtilisation() {
        let itemVolume = 0;

        _.forEach(this.items.toArray(), (item) => {
            itemVolume += item.getVolume();
        });

        return Math.round((itemVolume / this.box.getInnerVolume()) * 100);
    }

}
