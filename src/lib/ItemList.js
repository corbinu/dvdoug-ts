import Heap from "./heap";

function compare(itemA, itemB) {
    if (itemA.getVolume() > itemB.getVolume()) {
        return -1;
    } else if (itemA.getVolume() < itemB.getVolume()) {
        return 1;
    }

    return itemA.getWeight() - itemB.getWeight();

}

//
//Box packing (3D bin packing, knapsack problem)
//@package BoxPacker
//@author Doug Wright
//
//
//Compare elements in order to place them correctly in the heap while sifting up.
//@see \SplMaxHeap::compare()
//
//
//Get copy of this list as a standard PHP array
//@return array
//
export default class ItemList  extends Heap {
    constructor() {
        super(compare);
    }
}
