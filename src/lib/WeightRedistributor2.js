import * as _ from "lodash";

import Packer from "./Packer";
import PackedBoxList from "./PackedBoxList";

//
//Box packing (3D bin packing, knapsack problem)
//@package BoxPacker
//@author Doug Wright
//

//
//List of box sizes available to pack items into
//@var BoxList
//
//
//Constructor
//
//
//Given a solution set of packed boxes, repack them to achieve optimum weight distribution
//
//@param PackedBoxList $originalBoxes
//@return PackedBoxList
//
export default class WeightRedistributor {
    constructor(boxList) {
        this.boxes = _.clone(boxList);
        //this.logger = new NullLogger();
    }

    redistributeWeight(originalBoxes) {
        const targetWeight = originalBoxes.getMeanWeight();
        //this.logger.log(LogLevel.DEBUG, `repacking for weight distribution, weight variance ${originalBoxes.getWeightVariance()}, target weight ${targetWeight}`);
        const packedBoxes = new PackedBoxList();
        const overWeightBoxes = Array();
        const underWeightBoxes = Array();

        for (const packedBox of Object.values(_.clone(originalBoxes))) {
            const boxWeight = packedBox.getWeight();

            if (boxWeight > targetWeight) {
                overWeightBoxes.push(packedBox);
            } else if (boxWeight < targetWeight) {
                underWeightBoxes.push(packedBox);
            } else {
                //target weight, so we'll keep these
                packedBoxes.insert(packedBox);
            }
        }

        let tryRepack;

        do {
            //Keep moving items from most overweight box to most underweight box
            tryRepack = false;
            //this.logger.log(LogLevel.DEBUG, "boxes under/over target: " + underWeightBoxes.length + "/" + overWeightBoxes.length);

            for (const u of underWeightBoxes) {
                const underWeightBox = underWeightBoxes[u];
                //this.logger.log(LogLevel.DEBUG, "Underweight Box " + u);

                for (const o of overWeightBoxes) {
                    //For each item in the heavier box, try and move it to the lighter one
                    const overWeightBox = overWeightBoxes[o];
                    //this.logger.log(LogLevel.DEBUG, "Overweight Box " + o);
                    const overWeightBoxItems = overWeightBox.getItems().toArray();

                    for (const oi of overWeightBoxItems) {
                        //we may need a bigger box
                        const overWeightBoxItem = overWeightBoxItems[oi];
                        //this.logger.log(LogLevel.DEBUG, "Overweight Item " + oi);

                        if (underWeightBox.getWeight() + overWeightBoxItem.getWeight() > targetWeight) {
                            //skip if moving this item would hinder rather than help weight distribution
                            //this.logger.log(LogLevel.DEBUG, "Skipping item for hindering weight distribution");
                            continue;
                        }

                        const newItemsForLighterBox = _.clone(underWeightBox.getItems());

                        newItemsForLighterBox.insert(overWeightBoxItem);
                        const newLighterBoxPacker = new Packer();

                        newLighterBoxPacker.setBoxes(this.boxes);
                        newLighterBoxPacker.setItems(newItemsForLighterBox);
                        //this.logger.log(LogLevel.INFO, "[ATTEMPTING TO PACK LIGHTER BOX]");
                        const newLighterBox = newLighterBoxPacker.doVolumePacking().pop();

                        if (newLighterBox.getItems().size() === newItemsForLighterBox.size()) {
                            //new item fits
                            //now packed in different box
                            //we may be able to use a smaller box
                            //we did some work, so see if we can do even better
                            //this.logger.log(LogLevel.DEBUG, "New item fits");
                            delete overWeightBoxItems[oi];
                            const newHeavierBoxPacker = new Packer();

                            newHeavierBoxPacker.setBoxes(this.boxes);
                            newHeavierBoxPacker.setItems(overWeightBoxItems);
                            //this.logger.log(LogLevel.INFO, "[ATTEMPTING TO PACK HEAVIER BOX]");
                            const newHeavierBoxes = newHeavierBoxPacker.doVolumePacking();

                            if (newHeavierBoxes.length > 1) {
                                //found an edge case in packing algorithm that *increased* box count
                                //this.logger.log(LogLevel.INFO, "[REDISTRIBUTING WEIGHT] Abandoning redistribution, because new packing is less efficient than original");

                                return originalBoxes;
                            }

                            overWeightBoxes[o] = newHeavierBoxes.pop();
                            underWeightBoxes[u] = newLighterBox;
                            tryRepack = true;
                            overWeightBoxes.sort([packedBoxes, "reverseCompare"]);
                            underWeightBoxes.sort([packedBoxes, "reverseCompare"]);
                            break;
                        }
                    }
                }
            }
        } while (tryRepack);

        packedBoxes.insertFromArray(overWeightBoxes);
        packedBoxes.insertFromArray(underWeightBoxes);

        return packedBoxes;
    }

}
