//
//Box packing (3D bin packing, knapsack problem)
//@package BoxPacker
//@author Doug Wright
//

//
//@var Item
//
//
//@var int
//
//
//@var int
//
//
//@var int
//
//
//Constructor.
//@param Item $item
//@param int $width
//@param int $length
//@param int $depth
//
//
//Item
//
//@return Item
//
//
//Item width in mm in it's packed orientation
//
//@return int
//
//
//Item length in mm in it's packed orientation
//
//@return int
//
//
//Item depth in mm in it's packed orientation
//
//@return int
//
//
//Is this orientation stable (low centre of gravity)
//N.B. Assumes equal weight distribution
//
//@return bool
export default class OrientatedItem {
    constructor(item, width, length, depth) {
        this.item = item;
        this.width = width;
        this.length = length;
        this.depth = depth;
    }

    getItem() {
        return this.item;
    }

    getWidth() {
        return this.width;
    }

    getLength() {
        return this.length;
    }

    getDepth() {
        return this.depth;
    }

    isStable() {
        return this.getDepth() <= Math.min(this.getLength(), this.getWidth());
    }

}
