//
//Box packing (3D bin packing, knapsack problem)
//@package BoxPacker
//@author Doug Wright
//

//@var Item
//
//ItemTooLargeError constructor.
//
//@param string $message
//@param Item   $item
//
//
//@return Item
//
export default class ItemTooLargeError extends Error {
    constructor(message, item) {
        super(message);

        this.item = item;

        Error.captureStackTrace(this, ItemTooLargeError);

        this.name = "ItemTooLargeError";
    }

    getItem() {
        return this.item;
    }

}
