import * as _ from "lodash";

import ItemList from "./ItemList";
import BoxList from "./BoxList";
import WeightRedistributor from "./WeightRedistributor";
import PackedBoxList from "./PackedBoxList";
import ItemTooLargeError from "./ItemTooLargeError";
import VolumePacker from "./VolumePacker";

//
//Box packing (3D bin packing, knapsack problem)
//@package BoxPacker
//@author Doug Wright
//

//
//List of items to be packed
//@var ItemList
//
//
//List of box sizes available to pack items into
//@var BoxList
//
//
//Constructor
//
//
//Add item to be packed
//@param Item $item
//@param int  $qty
//
//
//Set a list of items all at once
//@param \Traversable|array $items
//
//
//Add box size
//@param Box $box
//
//
//Add a pre-prepared set of boxes all at once
//@param BoxList $boxList
//
//
//Pack items into boxes
//
//@return PackedBoxList
//
//
//Pack items into boxes using the principle of largest volume item first
//
//@throws ItemTooLargeError
//@return PackedBoxList
//
const MAX_BOXES_TO_BALANCE_WEIGHT = 12;

export default class Packer {
    constructor() {
        this.items = new ItemList();
        this.boxes = new BoxList();
        //this.logger = new NullLogger();
    }

    addItem(item, qty = 1) {
        for (let i = 0; i < qty; i++) {
            this.items.insert(_.clone(item));
        }

        //this.logger.log(LogLevel.INFO, `added ${qty} x ${item.getDescription()}`);
    }

    setItems(items) {
        if (items instanceof ItemList) {
            this.items = _.clone(items);
        } else {
            this.items = new ItemList();

            for (const item of Object.values(items)) {
                this.items.insert(item);
            }
        }
    }

    addBox(box) {
        this.boxes.insert(box);
        //this.logger.log(LogLevel.INFO, `added box ${box.getReference()}`);
    }

    setBoxes(boxList) {
        this.boxes = _.clone(boxList);
    }

    pack() {
        //If we have multiple boxes, try and optimise/even-out weight distribution
        let packedBoxes = this.doVolumePacking();

        if (packedBoxes.size() > 1 && packedBoxes.size() < MAX_BOXES_TO_BALANCE_WEIGHT) {
            const redistributor = new WeightRedistributor(this.boxes);

            //redistributor.setLogger(this.logger);
            packedBoxes = redistributor.redistributeWeight(packedBoxes);
        }

        //this.logger.log(LogLevel.INFO, `packing completed, ${packedBoxes.size()} boxes`);

        return packedBoxes;
    }

    doVolumePacking() {
        //Keep going until everything packed
        const packedBoxes = new PackedBoxList();

        while (this.items.size()) {
            //console.log("NEW PACK");
            //Loop through boxes starting with smallest, see what happens
            //Check iteration was productive
            const boxesToEvaluate = this.boxes.clone();
            const packedBoxesIteration = new PackedBoxList();

            while (!boxesToEvaluate.empty()) {
                const box = boxesToEvaluate.pop();
                //console.log("box");
                //console.log(JSON.stringify(box));
                const volumePacker = new VolumePacker(box, this.items.clone());

                //volumePacker.setLogger(this.logger);
                const packedBox = volumePacker.pack();

                if (packedBox.getItems().size()) {
                    //Have we found a single box that contains everything?
                    packedBoxesIteration.insert(packedBox);

                    if (packedBox.getItems().size() === this.items.size()) {
                        break;
                    }
                }
            }

            if (packedBoxesIteration.empty()) {
                throw new ItemTooLargeError(`Item ${this.items.top().getDescription()} is too large to fit into any box`, this.items.top());
            }

            const bestBox = packedBoxesIteration.top();

            const unPackedItems = _.differenceWith(this.items.toArray(), bestBox.getItems().toArray(), (item1, item2) => {
                return item1[Symbol.for("head-ts-id")] === item2[Symbol.for("head-ts-id")];
            });

            const unpackedItemList = new ItemList();

            for (const unpackedItem of Object.values(unPackedItems)) {
                unpackedItemList.insert(unpackedItem);
            }

            this.items = unpackedItemList;

            packedBoxes.insert(bestBox);
        }

        return packedBoxes;
    }

}
